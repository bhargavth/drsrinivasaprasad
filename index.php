<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Home | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <div class="main-content">
        <section class="section home-banner row-middle">
            <div class="inner-bg"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="banner-content">
                            <h1>We care your Heart</h1>
                            <p>Seeking for the permanent solution for your health problems? Get an appointment to find answers to all your queries from Dr. Srinivasa Prasad.</p>
                            <a title="Consult" class="btn btn-primary consult-btn appointment_link" href="javascript:void(0);">Book an Appointment</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="section about" data-scroll-index="1">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-9 col-xs-9 offset-md-0 about-image">
                        <!-- <img src="assets/img/Common Images/doctor-placeholder.png" alt="Dr Doctor Name" class="float-left pr-5"> -->
                        <img src="assets/img/dr_placeholder/7.jpeg" alt="Dr Doctor Name" class="float-left pr-5">
                    </div>
                    <div class="col-lg-9 col-md-12 mb-sm30 about-content">
                        <h2><span class="about_meet_text">Meet</span>Dr. Srinivasa Prasad</h2>
                        <h5>MBBS, MD, DM (Cardiology).</h5>
                        <p class="pb-20">Being one of the most crucial branches of medical science, cardiology deals with treating the heart of the patients. It takes utmost brilliance and skills to become a successful cardiologist. Thus, only a few people can traverse this path and win accolades in this field.</p>
                        <p>Dr Srinivasa Prasad B.V is one of such doctors who have brought new dimensions to the field of interventional cardiology.</p>
                        <a href="about.php" class="btn btn-primary see-all-btn"><span>View Profile</span></a>
                    </div>
                </div>
            </div>
        </section>

        
        <section class="section departments">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="header-title">Services</h3>
							<div class="line"></div>
                            <p>Holding years of experience and profound knowledge, Dr. Srinivasa Prasad has been successfully solving the issues of every client. To know more about his services, check below:</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="dept-box coronary">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Coronary Angiogram</a></h4>
                            <p>A procedure which makes use of X-rays and special devices to understand whether there is a restriction in the heart. An expert in performing this test, Dr Srivinasa Prasad provides accurate results from the tests.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dept-box ffr">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Fractional Flow Reserve</a></h4>
                            <p>FFR or Fractional Flow Reserve is a process to measure the pressure difference in blood in the coronary artery. This is one of the processes that Dr Prasad excels in.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dept-box oct">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Optical coherence tomography</a></h4>
                            <p>An intricate process that makes use of coherent light for imaging in micrometre resolution, Dr Srinivasa Prasad also makes use of the advanced OCT process to diagnose coronary artery diseases.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dept-box cath">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Cath Study</a></h4>
                            <p>Used in treating several heart conditions by inserting a catheter through the groin, the doctor is particularly skillful in performing cath study to help the patients in different ways.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dept-box iabp">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Intra Aortic Balloon Pump</a></h4>
                            <p>As the name suggests, the Intra Aortic Balloon Pump is a device that helps to increase the flow of blood through the aorta. Dr Prasad is widely acclaimed for treating patients with IABP to get the best results.</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="dept-box pace">
                            <div class="dept-img">
                                <a href="services.php"></a>
                            </div>
                            <h4><a href="services.php">Pacemaker</a></h4>
                            <p>A widely used electrical device used to generate electrical impulses to help the heart muscles to contract, the doctor installs both permanent as well as temporary pacemakers in patients.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="see-all">
                            <a href="services.php" class="btn btn-primary see-all-btn">See all Services</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Start Video Section -->
        <section class="section bg-video">
            <div class="bg-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center section_title text-white">
                            <h3 class="font-weight-bold mb-0">What Dr. Srinivasa Prasad Says</h3>
                            <div class="section_title_border">
                                <div class="f-border bg-white"></div>
                                <div class="s-border bg-white"></div>
                                <div class="f-border bg-white"></div>
                            </div>
                            <p class="sec_subtitle mx-auto mt-2">To know more about the heart disease and precautions to be taken for preventions of heart diseases, watch the below video.</p>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <a id="video-link" data-video-id="y-Q3HjMJzQlOM" class="video_presentation_play"><i class="mdi mdi-play presentation_icon"></i> </a>
                        </div>
                    </div>
                </div>
            </div>            
        </section>
        <!-- End Video Section -->

        <section class="section testimonials" id="testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="header-title">Testimonials</h3>
                            <div class="line"></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae risus nec dui venenatis dignissim. Aenean vitae metus in augue pretium ultrices. Duis dictum eget dolor vel blandit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="testimonial_slider" class="owl-carousel text-center">
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <img class="img-responsive" src="assets/img/Common Images/profile_placeholder.png" alt="" width="150" height="150">
                                    </div>
                                    <div class="testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                    <div class="testimonial-author">
                                        <h3 class="testi-user">- Patient 1</h3>
                                        <span>Leland, USA</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <img class="img-responsive" src="assets/img/Common Images/profile_placeholder.png" alt="" width="150" height="150">
                                    </div>
                                    <div class="testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                    <div class="testimonial-author">
                                        <h3 class="testi-user">- Patient 2</h3>
                                        <span>Abington, USA</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <img class="img-responsive" src="assets/img/Common Images/profile_placeholder.png" alt="" width="150" height="150">
                                    </div>
                                    <div class="testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                    <div class="testimonial-author">
                                        <h3 class="testi-user">- Patient 3</h3>
                                        <span>El Paso, USA</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <img class="img-responsive" src="assets/img/Common Images/profile_placeholder.png" alt="" width="150" height="150">
                                    </div>
                                    <div class="testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                    <div class="testimonial-author">
                                        <h3 class="testi-user">- Patient 4</h3>
                                        <span>Vero Beach, USA</span>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <img class="img-responsive" src="assets/img/Common Images/profile_placeholder.png" alt="" width="150" height="150">
                                    </div>
                                    <div class="testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                    <div class="testimonial-author">
                                        <h3 class="testi-user">- Patient 5</h3>
                                        <span>Tallahassee, USA</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Count Section Starts Here -->
        <section class="numbers section">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        <div class="item text-center mb-sm60">
                            <span class="icon">
                                <i class="ti-download"></i>
                            </span>
                            <h3 class="count">150</h3>
                            <h5>Happy Patients</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        <div class="item text-center mb-sm60">
                            <span class="icon">
                                <i class="ti-layers"></i>
                            </span>
                            <h3 class="count">21</h3>
                            <h5>Publications</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        <div class="item text-center">
                            <span class="icon">
                                <i class="ti-pencil-alt"></i>
                            </span>
                            <h3 class="count">11</h3>
                            <h5>Awards and Prizes</h5>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6">
                        <div class="item text-center mb-sm60">
                            <span class="icon">
                                <i class="ti-cup"></i>
                            </span>
                            <h3 class="count">10</h3>
                            <h5>Conference Presentations</h5>
                            
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- Count Section Ends Here -->

        <?php /* ?><section class="section blog_section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="section-header text-center">
                            <h3 class="header-title">Recent Blog</h3>
                            <div class="line"></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vitae risus nec dui venenatis dignissim. Aenean vitae metus in augue pretium ultrices. Duis dictum eget dolor vel blandit.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="padding: 0;">
                        <div id="blog_slider" class="owl-carousel text-center">
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <a href=""><img class="img-responsive" src="assets/img/blog-img.jpg" alt="" width="150" height="150"></a>
                                    </div>
                                    <div class="blog-title-author testimonial-author">
                                        <h3 class="testi-user"><a href="">Blog Title 1</a></h3>
                                        <span class="blog_date"><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</span> | <span class="blog_author">Author</span>
                                    </div>
                                    <div class="blog-text testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <a href=""><img class="img-responsive" src="assets/img/blog-img.jpg" alt="" width="150" height="150"></a>
                                    </div>
                                    <div class="blog-title-author testimonial-author">
                                        <h3 class="testi-user"><a href="">Blog Title 2</a></h3>
                                        <span class="blog_date"><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</span> | <span class="blog_author">Author</span>
                                    </div>
                                    <div class="blog-text testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <a href=""><img class="img-responsive" src="assets/img/blog-img.jpg" alt="" width="150" height="150"></a>
                                    </div>
                                    <div class="blog-title-author testimonial-author">
                                        <h3 class="testi-user"><a href="">Blog Title 3</a></h3>
                                        <span class="blog_date"><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</span> | <span class="blog_author">Author</span>
                                    </div>
                                    <div class="blog-text testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <a href=""><img class="img-responsive" src="assets/img/blog-img.jpg" alt="" width="150" height="150"></a>
                                    </div>
                                    <div class="blog-title-author testimonial-author">
                                        <h3 class="testi-user"><a href="">Blog Title 4</a></h3>
                                        <span class="blog_date"><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</span> | <span class="blog_author">Author</span>
                                    </div>
                                    <div class="blog-text testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="testimonial-list">
                                    <div class="testimonial-img">
                                        <a href=""><img class="img-responsive" src="assets/img/blog-img.jpg" alt="" width="150" height="150"></a>
                                    </div>
                                    <div class="blog-title-author testimonial-author">
                                        <h3 class="testi-user"><a href="">Blog Title 5</a></h3>
                                        <span class="blog_date"><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</span> | <span class="blog_author">Author</span>
                                    </div>
                                    <div class="blog-text testimonial-text">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> <?php */?>
    </div>

    <?php require("includes/footer.php"); ?>

    <!-- scrollIt -->
    <script src="assets/js/scrollIt.min.js"></script>

    <!-- jquery.waypoints.min -->
    <script src="assets/js/jquery.waypoints.min.js"></script>

    <!-- jquery.counterup.min -->
    <script src="assets/js/jquery.counterup.min.js"></script>

    <!-- owl carousel -->
    <script src="assets/js/owl.carousel.min.js"></script>

    <!-- jquery.magnific-popup js -->
    <script src="assets/js/jquery.magnific-popup.min.js"></script>

    <!-- YouTubePopUp.jquery -->
    <script src="assets/js/YouTubePopUp.jquery.js"></script>

    <!-- custom scripts -->
    <script src="assets/js/scripts.js"></script>

    <script src="assets/js/jquery-video-lightning.js"></script>
    <script>
        $(function() {
            $("#video-link").jqueryVideoLightning({
                autoplay: 1,
                backdrop_color: "#000",
                backdrop_opacity: 0.6,
                glow: 20,
                glow_color: "#000"
            });
        });
        // $(".play-1").yu2fvl({ minPaddingY: 200, minPaddingX: 200, vid: "YJM8vUFiCPw", ratio: 4/3 });
    </script>
</body>
</html>