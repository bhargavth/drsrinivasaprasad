<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Contact | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Contact</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="blog-view">
                            <article>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Contact</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">

                                    <div class="row clearfix">
                                        <div class="col-lg-12 map-frame">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.952319703975!2d77.58837061482137!3d12.910786090895787!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae150dfe1f3315%3A0x62d35cfe772c604!2s1758%2C+18th+Main+Rd%2C+Marenahalli%2C+2nd+Phase%2C+JP+Nagar%2C+Bengaluru%2C+Karnataka+560078!5e0!3m2!1sen!2sin!4v1543151434697" width="600" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                                            <p class="contact-cont"></p>
                                        </div>

                                        <!--Info Column-->
                                        <div class="info-column col-md-5 col-sm-12 col-xs-12">
                                            <div class="inner-column">
                                                <h3>Dr. Srinivasa Prasad</h3>
                                                <div class="text">Have any Queries? Let us know. We will clear it for you at the best.</div>
                                                <ul class="list-style-two style-two">
                                                    <li><span class="icon fa fa-map-marker"></span><strong>Office</strong>1758, 18th main, 9th cross,<br>
                                                    J.P.Nagar, II phase,<br>
                                                    Bangalore - 560078</li>
                                                    <li><span class="icon fa fa-envelope-o"></span><strong>Email</strong><a href="mailto:drbvsp@gmail.com">drbvsp@gmail.com</a></li>
                                                    <li><span class="icon fa fa-phone"></span><strong>Phone</strong><a href="tel:+91 9886689465">+91 9886689465</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        
                                        <!--Content Column-->
                                        <div class="content-column col-md-7 col-sm-12 col-xs-12">
                                            <div class="inner-column">
                                                <p>We are happy to hear from you. Please contact us using the form below:<br>Fields marked ( <span class="text-red">*</span> ) are required</p>
                                                <?php require("includes/contact-form.php"); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php // require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
    <?php require("includes/contact-validation.php"); ?>
</body>
</html>