<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>Online Appointment | Dr. Srinivasa Prasad</title>
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
        <!--[if lt IE 9]>
                    <script src="assets/js/html5shiv.min.js"></script>
                    <script src="assets/js/respond.min.js"></script>
            <![endif]-->
    </head>
    <body>
        <?php require("includes/header.php"); ?>
        <!-- Content -->
        <div class="main-content">
            <!-- Page Header -->
            <div class="page-header inner-banner">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="page-title">
                                <span>Online Appointment</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content inner-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="blog-view">
                                <article class="blog blog-single-post">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="section-header">
                                                <h3 class="header-title">Online Appointment</h3>
                                                <div class="line"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blog-content">
                                        <p>Seeking for the permanent solution for your health problems? Get an appointment to find answers to all your queries from Dr. Srinivasa Prasad.</p>
                                        <div class="account-box">
                                            <?php require("includes/appointment-form.php"); ?>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <?php require("includes/sidebar.php"); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php require("includes/footer.php"); ?>
        <?php require("includes/appointment-validation.php"); ?>
    </body>
</html>