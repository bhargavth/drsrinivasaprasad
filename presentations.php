<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Presentations | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Presentations</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Presentations</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">
                                    <ul>
                                        <li>He had the pleasure of presenting a baffling case of myocarditis at Trivandrum Cardiology Club in the year 2014.</li> 
                                        <li>Dr Prasad also showcased a complicated and unseen case of postpartum cardiomyopathy at the International Heart Failure Society Conference back in 2014. </li>
                                        <li>Presented an interesting interventional case of – retrieval of broken catheter, in Back to Basics session, organized by SCTIMST, Dec 2014</li>
                                        <li>Presented an unique case of FFR assessment of multiple tandem lesions and collateral vessels at International Coronary Imaging and Physiology Conference May 2015</li>
                                        <li>Presented an interesting interventional cases (2) of – Coronary perforation, in Back to Basics session, organized by SCTIMST, Dec 2015</li>
                                        <li>Presented an unique case of Chronic total occlusion of right coronary artery – discussing the retrograde approach in SCAI’s advanced learning session in National Intervention Council Meet, held at Hyderabad in April 2016.</li>
                                        <li>E-Poster presentation of an unique case demonstrating assessment of adequacy of collateral supply, at National Interventional Council Meet, held at Hyderabad in April 2016</li>
                                        <li>Presented a case of left atrial appendage closure discussing the technicalities with modifications to suit the need of the case - in 4th  annual conference of Indian College of Cardiology – Kerala chapter, July 2016</li>
                                        <li>Presented a paper of “FFR-IVUS correlation in intermediate coronary artery disease” at an annual conference of Interventional cardiology council which was held in Kerala in 2016.</li>
                                        <li>In September 2016, he put forward a case of left atrial appendage closure demonstrating that it is oversizing and not undersizing that leads to unstable device position and potential device displacement, at Indian College of Cardiology annual conference 2016 which took place in Goa. </li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
</body>
</html>