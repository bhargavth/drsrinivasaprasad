<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Publications | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Publications</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Publications</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">
                                    <p><strong>Here are a few of the many notable works that Dr. Srinivasa Prasad worked on:</strong></p>
                                    <ol class="publications-list">
                                        <li><p>Radha K. Dhiman, Srinivasa Prasad, Ajay Duseja, Yogesh K. Chawla, Arpita Sharma, and Ritesh garwal. Lactulose improves cognitive functions and health - related quality of life in patients with cirrhosis who have minimal hepatic encephalopathy. (Abstract), LIVER. Journal of Gastroenterology and Hepatology. 2006; 21: A442 – A472. doi: 10.1111/j.1440 - 1746.2006.04805.</p><span> - 2006</span></li>
                                        <li><p>Srinivasa Prasad, Radha K. Dhiman, Ajay Duseja, Yogesh K. Chawla, Arpita Sharma, and Ritesh Agarwal. Lactulose improves cognitive functions and health - related quality of life in patients with cirrhosis who have minimal hepatic encephalopathy. Hepatology. 2007 Mar;45(3):549 - 59.</p><span> - 2007</span></li>
                                        <li><p>Srinivasa Prasad, Sheldon M. Singh, Jacob S. Koruth, Krishna Kumar Mohanan Nair. Nonpharmacological treatment of Atrial Fibrillation – Percutaneous Left atrial appendage closure: Practical guide to practicising cardiologist. Kerala Heart Journal, 2015, Volume 5, No 1;19 - 24.</p><span>- 2015</span></li>
                                        <li><p>Mukund A. Prabhu, Narayanan Namboodiri, Srinivas Prasad BV, Abhilash SP, Anees Thajudeen, Kumar V.K. Ajit. Acute outcome of treating patients admitted with electrical storm in a tertiary care centre, Indian Pacing and Electrophysiology Journal, November – December 2015, Vol.15(6):286 – 290, doi:10.1016/j.ipej.2016.03.002</p><span>2015</span></li>
                                        <li><p>Mukund A Prabhu, Srinivas Prasad BV, Anees Thajudeen and Narayanan Namboodiri. Persistent Atrial Standstill in Acute Myocarditis. Indian Pediatrics. Volume 53;Feb 2016;162 - 164</p><span>2016</span></li>
                                        <li><p>Mukund A Prabhu, Srinivas Prasad BV, Anees Thajudeen and Narayanan Namboodiri. Bundle Branch VT in case of isolated left sided endomyocardial fibrosis – a rare phenomenon. Indian Heart J. (2016), http://dx.doi.org/10.1016/j.ihj.2016.02.005. (in press)</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad, Harikrishnan S, Sanjay G, Bijulal S, Krishna Kumar M, Abhilash SP, Jaganmohan Tharakan, Ajit Kumar VK. Long - term Clinical Outcomes of  patients with stable angina who underwent FFR evaluation  in intermediate coronary lesionS – COFFRS Study. Indian Heart Journal (Accepted for publication; Manuscript No: IHJ - D - 16 - 00145R4)</p><span>2016</span></li>
                                        <li><p>Mukund A Prabhu. Srinivas Prasad B V, Abhilash S P, Anees Thajudeen, Balasubramoniam K R, Narayanan Namboodiri. Left sympathetic cardiac denervation in managing electrical storm: Acute outcome and long term follow up. Journal of Interventional Cardiac Electrophysiology 2016. DOI 10.1007/s10840 - 016 - 0153 - 2</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad BV, Narayanan Namboodiri, Anees Thajudheen, Gurbhej Singh, Mukund A Prabhu, Abhilash SP, Krishnakumar Mohanan Nair, Aamir Rashid, Ajit Kumar VK, Jaganmohan Tharakan. Flecainide Challenge Test : Predictors of inducible Type 1 Brugada Pattern in patients without spontaneous Type 1 Brugada Pattern. Indian Pacing and Electrophysiology March – April 2016, Vol.16(2):53 – 58. DOI: 10.1016/j.ipej.2016.06.001</p><span>2016</span></li>
                                        <li><p>Mukund A. Prabhu, Anees Thajudeen, Ajit Kumar VK, Jaganmohan Tharakan, Srinivas Prasad BV, Narayanan Namboodiri. Radiofrequency Ablation of Left Atrial Reentrant Tachycardias in Rheumatic Mitral Valve Disease: A Case Series. Pacing and Clinical Electrophysiology 2016. doi:10.1111/pace.12912</p><span>2016</span></li>
                                        <li><p>Krishnakumar Mohanan Nair, Narayanan Namboodiri, Bharat Banavalikar, Arun Gopalakrishnan, Srinivasa Prasad, Anees Thajudheen, Ajit Kumar VK, Jaganmohan Tharakan. Iatrogenic pneumopericardium after pericardiocentesis. The Journal of invasive cardiology 12/2016; 28(12):E225 - E226.</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad, Gurbhej Singh, Deepa S Kumar, Sivasankaran S, Ajit Kumar VK. Ectopic left ventricular apical diverticulum. European Heart Journal 2016. DOI: 10.1093/eurheartj/ehw519</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad, Harikrishnan S, Sanjay G, Abhilash SP, Bijulal S, Jaganmohan Tharakan, Ajit Kumar VK. FFR - IVUS Correlation Study (FFRICS) in non - LMCA intermediate coronary lesions. ICC - CON 2016 supplement. Journal of Indian College of Cardiology 2016 (Abstract – in press)</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad, Bijulal S, Sanjay G, Harikrishnan S, Ranjan Shetty,Sivasankaran S, Ajit Kumar VK. A case of left atrial appendage – technical modification in a shallow appendage. 2016 ICC - CON 2016 supplement. Journal of Indian College of Cardiology 2016 (Abstract – in press)</p><span>2016</span></li>
                                        <li><p>Srinivasa Prasad, Narayanan Namboodiri. Journal scan (Jan & Feb 2015) – compilation of major electrophysiology articles from major Journals for IHRS. http://ihrs.in/journal - scan.aspx</p><span>2015</span></li>
                                    </ol>
                                    <h4>Others : Submitted for publication / Work in progress</h4>
                                    <ol class="publications-list">
                                        <li><p>Srinivasa Prasad, Harikrishnan S. Collateral Fractional Flow Reserve – impact in clinical context. (submitted for publication)</p><span>2017</span></li>
                                        <li><p>Srinivasa Prasad, Mahim Saran, Gurbhej Singh. Pattern recognition in congenital heart disease :  “Second QRS” pattern of Ebstein anomaly. (submitted for publication)</p><span>2017</span></li>
                                        <li><p>Srinivasa Prasad, Krishnakumar Mohanan Nair, Sanjay G, Harikrishnan Sivadasanpillai, Ajitkumar VK. Absent Brockenbrough phenomenon in HOCM. (submitted for publication)</p><span>2017</span></li>
                                        <li><p>Srinivasa Prasad, Gurbhej Singh, Narayanan Namboodiri, Mukund A Prabhu, Anees Thajudheen, Ajit Kumar VK, Thomas Titus. Cardiac Sarcoidosis – does outcome varies  with presentation as bradyarrhythmia versus tachyarrhythmia ? An experience from cardiac tertiary centre. (Work in progress)</p><span>2017</span></li>
                                        <li><p>Srinivasa Prasad BV, Narayanan Namboodiri, Anees Thajudheen, Gurbhej Singh, Mukund A Prabhu, Abhilash SP, Krishnakumar Mohanan Nair, Aamir Rashid, Ajit Kumar VK. Superior lead placement helps in improving sensitivity of oral flecainide challenge test. (submitted for publication)</p><span>2017</span></li>
                                        <li><p>Harikrishnan S, Rajesh Muralidharan, Sanjay G, Srinivasa Prasad, Ajit Kumar VK, Thomas Titus, JM Tharakan, Sivasankaran S. 20 year outcome after coronary stent implantation: follow - up of 100 consecutive patients who underwent first generation bare - metal stent implantation in a tertiary care centre in India. (Submitted for publication)</p><span>2017</span></li>
                                    </ol>
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
</body>
</html>