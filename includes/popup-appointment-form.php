<style type="text/css">
    #popup_appointment_bg{
        background: #000;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        opacity: .7;
        z-index: 11;
        display: none;
    }
    #popup_appointment_container{
        position: fixed;
        top: 50%;
        left: 50%;
        margin:;
        width: 30%;
        z-index: 12;
        margin-left: -15%;
        margin-top: -18%;
        display: none;
    }
    #popup_appointment_container .appointment{
        padding: 20px;
        background: #fff;
    }
    @media screen and (max-width:639px){
        #popup_appointment_container {
            width: 90%;
            margin-left: -45%;
            top: 60px;
            margin-top: 0;
            z-index: 99;
        }
        #popup_appointment_container .form-group {
            margin-bottom: 5px;
        }
    }
</style>
<div id="popup_appointment_bg"></div>
<div id="popup_appointment_container">
    <?php require("appointment-form.php"); ?>
    <?php require("appointment-validation.php"); ?>
</div>

<script type="text/javascript">
    
    // Script for the popup appointment form
    $(".appointment_link").click(function(){
        $("#popup_appointment_bg,#popup_appointment_container").show();
    });

    $("#popup_appointment_bg").click(function(){
        $("#popup_appointment_bg,#popup_appointment_container").hide();
    })

    $(document).on('keydown',function(e){
        if(e.keyCode === 27){ // ESC
            $("#popup_appointment_bg,#popup_appointment_container").hide();
        }
    });

</script>