<aside class="col-sm-4 sidebar-right">

    <div class="dr-profile-sidebar col-sm-12">
        <div class="sidebar-dr-image col-sm-3">
            <a href="about.php"><img src="assets/img/doctor-placeholder.png"></a>
        </div>
        <div class="sidebar-dr-content col-sm-9">
            <h4><a href="about.php">Dr. Srinivasa Prasad</a><span class="qualification">MBBS, MD, DM (Cardiology).</span></h4>
            <p>Dr Srinivasa Prasad B.V is one of such doctors who have brought new dimensions to the field of interventional cardiology</p>
            <div class="view-profile"><a href="about.php" class="btn btn-primary see-all-btn">View Profile</a></div>
        </div>
    </div>

    <?php /* if(basename($_SERVER['PHP_SELF']) != 'online-appointment.php') { ?>
    <div class="account-box">
        <h5>Book an Appointment</h5>
        <div class="appointment">
            <div class="tab-pane" id="online-consultation">
                <form>
                    <div class="form-group">
                        <label>Name <span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Mobile <span class="text-red">*</span></label>
                        <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Choose the Date <span class="text-red">*</span></label>
                        <div class="cal-icon">
                            <input type="text" class="form-control datetimepicker" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Message (optional)</label>
                        <textarea class="form-control"></textarea>
                    </div>
                    <div class="form-group text-center m-b-0">
                        <button class="btn btn-primary account-btn" type="submit">Confirm Booking</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php } */ ?>

    <!-- Services -->
    <div class="widget category-widget col-sm-12 sidebar-services">
        <h5>Services</h5>
        <ul class="categories">
            <li><a href="services.php">Coronary Angiogram</a></li>
            <li><a href="services.php">Fractional Flow Reserve</a></li>
            <li><a href="services.php">Optical coherence tomography</a></li>
            <li><a href="services.php">Cath Study</a></li>
            <li><a href="services.php">Intra Aortic Balloon Pump</a></li>
            <li><a href="services.php">Pacemaker</a></li>
        </ul>
        <div class="view-all"><a href="services.php" class="btn btn-primary see-all-btn">View All Services</a></div>
    </div>

    <!-- Latest Posts -->
    <?php /*<div class="widget post-widget">
        <h5>Latest Posts</h5>
        <ul class="latest-posts">
            <li>
                <div class="post-thumb">
                    <a href="blog-details.html">
                        <img class="img-responsive" src="assets/img/blog-img.jpg" alt="">
                    </a>
                </div>
                <div class="post-info">
                    <h4>
                        <a href="blog-details.html">Blog Title 1</a>
                    </h4>
                    <p><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</p>
                </div>
            </li>
            <li>
                <div class="post-thumb">
                    <a href="blog-details.html">
                        <img class="img-responsive" src="assets/img/blog-img.jpg" alt="">
                    </a>
                </div>
                <div class="post-info">
                    <h4>
                        <a href="blog-details.html">Blog Title 2</a>
                    </h4>
                    <p><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</p>
                </div>
            </li>
            <li>
                <div class="post-thumb">
                    <a href="blog-details.html">
                        <img class="img-responsive" src="assets/img/blog-img.jpg" alt="">
                    </a>
                </div>
                <div class="post-info">
                    <h4>
                        <a href="blog-details.html">Blog Title 3</a>
                    </h4>
                    <p><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</p>
                </div>
            </li>
            <li>
                <div class="post-thumb">
                    <a href="blog-details.html">
                        <img class="img-responsive" src="assets/img/blog-img.jpg" alt="">
                    </a>
                </div>
                <div class="post-info">
                    <h4>
                        <a href="blog-details.html">Blog Title 4</a>
                    </h4>
                    <p><i aria-hidden="true" class="fa fa-calendar"></i> Dec 6, 2018</p>
                </div>
            </li>
        </ul>
    </div>*/?>

</aside>