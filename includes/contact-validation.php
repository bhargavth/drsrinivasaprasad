<script>

    $(document).ready(function () {
        if ($('.error-style').length) {
            $('html, body').animate({
                scrollTop: $('.page-title span').offset().top + 500
            }, 'slow');
        }
    });

    $(document).on('click', '#confirm-contact1', function () {
        var error = 0;

        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();

        var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,3}))$/;
        var phoneRegex = /^[0-9]{10}$/g;

        $('.error-style').removeClass('error-style');
        $('.error-message').html('');

        if ($.trim(name) == '') {
            error = 1;
            $('#name').addClass('error-style')
            $('.name-error').html('Please enter your name.')
        }

        if ($.trim(email) == '') {
            error = 1;
            $('#email').addClass('error-style')
            $('.email-error').html('Please enter your email.')
        } else if (!email.match(emailRegex)) {
            error = 1;
            $("#email").addClass("error-style");
            $(".email-error").html("Enter valid email.");
        }

        if ($.trim(mobile) == '') {
            error = 1;
            $('#mobile').addClass('error-style')
            $('.mobile-error').html('Please enter your mobile.')
        } else if (!mobile.match(phoneRegex)) {
            error = 1;
            $("#mobile").addClass("error-style");
            $(".mobile-error").html("Enter valid mobile number.");
        }

        if (error) {
            return false;
        }
    });

</script>