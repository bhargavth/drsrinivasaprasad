<!-- Preloader -->
<!--<div class="preloader"></div>-->

<!-- Header -->
<header class="header">
    
    <!-- Header Top End -->
    <div class="header-top">

        <!-- Header Top -->
        <div class="top-header">
            <div class="container">
                <div class="auto-container">
                    <div class="clearfix">
                        
                        <!--Top Left-->
                        <div class="top-left">
                            <ul class="links clearfix">
                                <li>Call Us: <a href="tel:+91 9886689465">+91 9886689465</a></li>
                                <li>Email: <a href="mailto:drbvsp@gmail.com">drbvsp@gmail.com</a></li>
                            </ul>
                        </div>
                        
                        <!--Top Right-->
                        <div class="top-right clearfix">
                                                    
                            <div class="login-register">
                                <a href="javascript:void(0);" class="appointment_link">Book an Appointment</a>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>



        <div class="container logo-container">
            <div class="row">
                <div class="col-md-4 pull-left">
                    <div class="logo">
                        <a href="index.php"><h2>Dr. Srinivasa Prasad</h2><h4>Cardiologist</h4></a>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="navbar-collapse collapse" id="navbar">

                        <ul class="nav navbar-nav main-nav pull-right navbar-right">
                            <li class="nav-item">
                                <a class="nav-link active" href="index.php">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="dropdown-toggle" data-toggle="dropdown">About <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a class="nav-link" href="about.php">Dr. Srinivas Prasad</a></li>
                                    <li><a class="nav-link" href="experience.php">Experience</a></li>
                                    <li><a class="nav-link" href="awards.php">Awards</a></li>
                                    <?php /* <li><a class="nav-link" href="presentations.php">Presentations</a></li> */ ?>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="services.php">Services</a>
                            </li>
                            <?php /* <li class="nav-item">
                                <a class="dropdown-toggle" href="online-appointment.php">Appointments <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a class="nav-link" href="general-appointment.php">General Appointment</a></li>
                                    <li><a class="nav-link" href="online-appointment.php">Online Appointment</a></li>
                                </ul>
                            </li> */?>
                            <li class="nav-item">
                                <a class="nav-link" href="presentations.php">Presentations</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="publications.php">Publications</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="blog.php">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="contact.php">Contact</a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Mobile Header -->
<header class="mobile-header">
    <div class="panel-control-left"><a class="toggle-menu" href="#side_menu"><i class="fa fa-bars"></i></a></div>
    <div class="page_title">
        <a href="index.php"><h2>Dr. Srinivasa Prasad</h2><h4>Cardiologist</h4></a>
    </div>
</header>

<!-- Mobile Sidebar -->
<div class="sidebar sidebar-menu" id="side_menu">
    <div class="sidebar-inner slimscroll">
        <a id="close_menu" href="#"><i class="fa fa-close"></i></a>
        <ul class="mobile-menu-wrapper" style="display: block;">
            <li class="active">
                <div class="mobile-menu-item clearfix">
                    <a href="index.php">Home</a>
                </div>
            </li>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="about.php">About <i class="fa fa-chevron-down menu-toggle"></i></a>
                </div>
                <ul class="mobile-submenu-wrapper" style="display: none;">
                    <li><a href="about.php">Dr. Srinivas Prasad</a></li>
                    <li><a href="experience.php">Experience</a></li>
                    <li><a href="awards.php">Awards</a></li>
                </ul>
            </li>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="services.php">Services</a>
                </div>
            </li>
            <?php /* <li>
                <div class="mobile-menu-item clearfix">
                    <a href="online-appointment.php">Appointments</a>
                </div>
            </li> */ ?>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="presentations.php">Presentations</a>
                </div>
            </li>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="publications.php">Publications</a>
                </div>
            </li>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="blog.php">Blog</a>
                </div>
            </li>
            <li>
                <div class="mobile-menu-item clearfix">
                    <a href="contact.php">Contact</a>
                </div>
            </li>
        </ul>
    </div>
</div>