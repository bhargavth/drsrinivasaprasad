<?php
if (!empty($_POST)) {


    if (empty($_POST['name'])) {
        $error['name'] = "Please enter your name.";
    }

    if (empty($_POST['email'])) {
        $error['email'] = "Please enter your email.";
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error['email'] = "Enter valid email.";
    }

    if (empty($_POST['mobile'])) {
        $error['mobile'] = "Please enter your mobile.";
    } else if (!preg_match('/^\d{10}$/', $_POST['mobile'])) {
        $error['mobile'] = "Enter valid mobile number.";
    }

    if (empty($_POST['date'])) {
        $error['date'] = "Please enter your date.";
    }

    if (!empty($error)) {
        $value['name'] = $_POST['name'] ? $_POST['name'] : "";
        $value['email'] = $_POST['email'] ? $_POST['email'] : "";
        $value['mobile'] = $_POST['mobile'] ? $_POST['mobile'] : "";
        $value['date'] = $_POST['date'] ? $_POST['date'] : "";
    } else {
        $data['belongsTo'] = 4;
        $data['patientName'] = $_POST['name'];
        $data['patientEmail'] = $_POST['email'];
        $data['patientMobile'] = $_POST['mobile'];
        $data['appointmentDateTime'] = date("Y-m-d H:i:s", strtotime(str_replace("/", "-", $_POST['date'])));
        $data['deviceIp'] = $_SERVER['REMOTE_ADDR'];

        $url = "http://localhost/projects/backend/api/appointments/makeAppointment";
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_URL, $url);
        $res = curl_exec($handle);
    }
}
?>
<div class="appointment">
    <div class="tab-pane" id="online-consultation">
        <form method="post" action="includes/appointment-form.php">
            <div class="form-group">
                <label>Name <span class="text-red">*</span></label>
                <input type="text" class="form-control <?= !empty($error['name']) ? "error-style" : "" ?>" name="name" id="name" value="<?= !empty($value['name']) ? $value['name'] : "" ?>"/>
                <span class="error-message name-error"><?= !empty($error['name']) ? $error['name'] : "" ?></span>
            </div>
            <div class="form-group">
                <label>Email <span class="text-red">*</span></label>
                <input type="text" class="form-control <?= !empty($error['email']) ? "error-style" : "" ?>" name="email" id="email" value="<?= !empty($value['email']) ? $value['email'] : "" ?>"/>
                <span class="error-message email-error"><?= !empty($error['email']) ? $error['email'] : "" ?></span>
            </div>
            <div class="form-group">
                <label>Mobile <span class="text-red">*</span></label>
                <input type="text" class="form-control <?= !empty($error['mobile']) ? "error-style" : "" ?>" name="mobile" id="mobile" value="<?= !empty($value['mobile']) ? $value['mobile'] : "" ?>"/>
                <span class="error-message mobile-error"><?= !empty($error['mobile']) ? $error['mobile'] : "" ?></span>
            </div>
            <div class="form-group">
                <label>Choose the Date <span class="text-red">*</span></label>
                <div class="cal-icon">
                    <input type="text" class="form-control datetimepicker <?= !empty($error['date']) ? "error-style" : "" ?>" name="date" id="date" value="<?= !empty($value['date']) ? $value['date'] : "" ?>"/>
                    <span class="error-message date-error"><?= !empty($error['date']) ? $error['date'] : "" ?></span>
                </div>
            </div>
            <div class="form-group">
                <label>Message (optional)</label>
                <textarea class="form-control" name="message"></textarea>
            </div>
            <div class="form-group text-center m-b-0">
                <button class="btn btn-primary account-btn" id="confirm-appointment" type="submit">Confirm Booking</button>
            </div>
        </form>
    </div>
</div>