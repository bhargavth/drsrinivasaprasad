<?php
if (!empty($_POST)) {


    if (empty($_POST['name'])) {
        $error['name'] = "Please enter your name.";
    }

    if (empty($_POST['email'])) {
        $error['email'] = "Please enter your email.";
    } else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error['email'] = "Enter valid email.";
    }

    if (empty($_POST['mobile'])) {
        $error['mobile'] = "Please enter your mobile.";
    } else if (!preg_match('/^\d{10}$/', $_POST['mobile'])) {
        $error['mobile'] = "Enter valid mobile number.";
    }

    if (!empty($error)) {
        $value['name'] = !empty($_POST['name']) ? $_POST['name'] : "";
        $value['email'] = !empty($_POST['email']) ? $_POST['email'] : "";
        $value['mobile'] = !empty($_POST['mobile']) ? $_POST['mobile'] : "";
    } else {
        $data['belongsTo'] = 4;
        $data['name'] = $_POST['name'];
        $data['email'] = $_POST['email'];
        $data['mobile'] = $_POST['mobile'];
        $data['message'] = $_POST['message'];
        $data['deviceIp'] = $_SERVER['REMOTE_ADDR'];

        $url = "http://localhost/projects/backend/api/contactUs/add";
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_URL, $url);
        $res = curl_exec($handle);
    }
}
?>
<!-- Default Form -->
<div class="contact-form default-form">
    <form method="post" action="contact.php" id="contact-form" novalidate="novalidate">
        <div class="form-group">
            <input type="text" placeholder="Name *" class="<?= !empty($error['name']) ? "error-style" : "" ?>" name="name" id="name" value="<?= !empty($value['name']) ? $value['name'] : "" ?>">
            <span class="error-message name-error"><?= !empty($error['name']) ? $error['name'] : "" ?></span>
        </div>

        <div class="form-group">
            <input type="text" placeholder="Email *" class="<?= !empty($error['email']) ? "error-style" : "" ?>" name="email" id="email" value="<?= !empty($value['email']) ? $value['email'] : "" ?>">
            <span class="error-message email-error"><?= !empty($error['email']) ? $error['email'] : "" ?></span>
        </div>

        <div class="form-group">
            <input type="text" placeholder="Mobile *" class="<?= !empty($error['mobile']) ? "error-style" : "" ?>" name="mobile" id="mobile" value="<?= !empty($value['mobile']) ? $value['mobile'] : "" ?>">
            <span class="error-message mobile-error"><?= !empty($error['mobile']) ? $error['mobile'] : "" ?></span>
        </div>

        <div class="form-group">
            <textarea name="message" placeholder="Message"></textarea>
        </div>

        <div class="form-group">
            <button class="theme-btn btn-style-two" type="submit" id="confirm-contact" name="submit-form">Submit now</button>
        </div>
    </form>
</div>  