<!-- Footer -->
<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-3">
                    <div class="footer-widget">
                        <h4 class="footer-title">Location</h4>
                        <div class="about-clinic">
                            <p><strong>Address:</strong><br>
                                1758, 18th main, 9th cross,<br>
                                J.P.Nagar, II phase, <br>
                                Bangalore - 560078</p>
                            <p class="m-b-0"><strong>Phone</strong>: <a href="tel:+91 9886689465">+91 9886689465</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-3 quicklinks">
                    <div class="footer-widget">
                        <h4 class="footer-title">Quicklinks</h4>
                        <ul class="footer-menu">
                            <li><a href="about.php">About</a></li>
                            <li><a href="services.php">Services</a></li>
                            <li><a href="testimonials.php">Testimonials</a></li>
                            <li><a href="publications.php">Publications</a></li>
                            <li><a href="blog.php">Blog</a></li>
                            <li><a href="contact.php">Contact</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3">
                    <div class="footer-widget">
                        <h4 class="footer-title">Follow Us</h4>
                        <div class="appointment-btn">
                            <p>Want to know more about us? Stay updated with our daily happenings and activities just by connecting with us in any of these platforms or all.</p>
                            <ul class="social-icons clearfix">
                                <li><a href="#" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#" target="_blank" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" target="_blank" title="Youtube"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="copy-text text-center">
                            <p>&copy; 2018 <a href="#">Dr. Srinivasa Prasad</a>. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="sidebar-overlay" data-reff="#side_menu"></div>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/moment.min.js"></script>
<script type="text/javascript" src="assets/js/select2.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="assets/js/theme.js"></script>

<?php
require("popup-appointment-form.php");
?>