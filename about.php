<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>About | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Dr. Srinivas Prasad</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Dr. Srinivas Prasad</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">
                                    <div class="about-content-image">
                                        <img src="assets/img/dr_placeholder/7.jpeg" alt="Dr Doctor Name" class="float-left pr-5">
                                    </div>
                                    <p>Being one of the most crucial branches of medical science, cardiology deals with treating the heart of the patients. It takes utmost brilliance and skills to become a successful cardiologist. Thus, only a few people can traverse this path and win accolades in this field. </p>
                                    <p>Dr Srinivasa Prasad B.V is one of such doctors who have brought new dimensions to the field of interventional cardiology. With the aim of achieving sustainable professional growth and maintaining a steady learning curve, Dr Srinivasa Prasad works hard to create technology-oriented medical solutions for treating the patients.
                                    </p>
                                    <p>After graduating from PGIMER, Dr Prasad had also practised as a consultant physician in order to enhance his own skills. After joining SCTMIST for fellowship and residency, Dr Srinivasa Prasad had won countless awards, accolades and recognition for his brilliance. Currently associated with Chinmaya Narayana Hospital as consultant interventional cardiologist, the doctor provides countless services to help the patients and assure them about living a happy life.</p>
                                    <h4>Qualification</h4>
                                    <ul>
                                        <li>MBBS</li>
                                        <li>MD (Internal Medicine)</li>
                                        <li>DM (Cardiology) </li>
                                        <li>Diploma in BIOSTATICS</li>
                                        <li>Fellow- Interventional Cardiology (post DM)</li>
                                    </ul>

                                    <h4>Field of Expertise:</h4>
                                    <p><strong>Performs minimally invasive and therapeutic procedures:</strong></p>
                                    <ul>
                                        <li>Consultant Interventional Cardiologist.</li> 
                                        <li>Consultant Cardiologist.</li>
                                        <li>Consultant Physician.</li>
                                        <li>Ace Assistant Professor of the Department of Medicine.</li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
</body>
</html>