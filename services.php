<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Services | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Services</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="content service-list inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="section-header">
                                    <h3 class="header-title">Featured Services</h3>
        							<div class="line"></div>
                                </div>
                            </div>
                        </div>
                        <p>Holding years of experience and profound knowledge, Dr. Srinivasa Prasad has been successfully solving the issues of every client. To know more about his services, check below.</p>
                        <p><strong>Cardiology:</strong> A branch of medicine that specializes in diagnosing and treating diseases of the heart, blood vessels, and circulatory system. These diseases include coronary artery disease, heart rhythm problems, and heart failure.</p>
                        <p><strong>Interventional cardiology</strong> is a branch of cardiology that deals specifically with the catheter based treatment of structural heart diseases. A large number of procedures can be performed on the heart by catheterization. This most commonly involves the insertion of a sheath into the femoral artery (but, in practice, any large peripheral artery or vein) and cannulating the heart under X-ray visualization (most commonly fluoroscopy).</p>
                        <p>The main advantages of using the interventional cardiology or radiology approach are the avoidance of the scars and pain, and long post-operative recovery.</p>
                        <p><strong>Here are a few services that Dr Srivinasa Prasad offers:</strong></p>
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-coronary-angiography-left-coronary-angiography-1109742584.jpg">
                            </div>

                            <div class="col-lg-8 clear-fix">
                                <h5>Coronary Angiogram: </h5>
                                <p>A procedure which makes use of X-rays and special devices to understand whether there is a restriction in the heart. An expert in performing this test, Dr Srivinasa Prasad provides accurate results from the tests. </p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Fractional Flow Reserve (FFR): </h5>
                                <p>FFR or Fractional Flow Reserve is a process to measure the pressure difference in blood in the coronary artery. This is one of the processes that Dr Prasad excels in. </p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <!-- <img src="https://image.shutterstock.com/z/stock-photo-optical-coherence-tomography-oct-image-of-eye-in-the-patient-418835149.jpg"> -->
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Optical Coherence Tomography (OCT): </h5>
                                <p>An intricate process that makes use of coherent light for imaging in micrometre resolution, Dr Srivinasa Prasad also makes use of the advanced OCT process to diagnose coronary artery diseases. </p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Cath Study: </h5>
                                <p>Used in treating several heart conditions by inserting a catheter through the groin, the doctor is particularly skillful in performing cath study to help the patients in different ways.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-ekg-monitor-in-intra-aortic-balloon-pump-machine-in-icu-on-blur-background-brain-waves-in-1075878368.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Intra Aortic Balloon Pump (IABP): </h5>
                                <p>As the name suggests, the Intra Aortic Balloon Pump is a device that helps to increase the flow of blood through the aorta. Dr Prasad is widely acclaimed for treating patients with IABP to get the best results.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-pacemaker-307066787.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Pacemaker: </h5>
                                <p>A widely used electrical device used to generate electrical impulses to help the heart muscles to contract, the doctor installs both permanent as well as temporary pacemakers in patients. </p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-vector-pacemaker-icd-implantable-cardioverte-defibrillator-pulse-generator-stimulate-of-heart-prevent-715472737.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Implantable Cardioverter Defibrillator (ICD): </h5>
                                <p>Dr Prasad is also highly skilled in implanting ICD devices which help in defibrillation, cardioversion and heart pacing in patients. </p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-x-ray-of-implantable-cardiac-resynchronization-therapy-defibrillator-crt-d-with-leads-ra-lead-602565884.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Cardiac Resynchronization Therapy (CRT):</h5> 
                                <p>A measure to treat the patients experiencing heart failure, cardiac resynchronization therapy or CRT makes use of electrodes to coordinate the heart functions. Dr Srinivasa Prasad is one of the few doctors considered to be a maestro in providing this service.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-heart-valve-surgery-for-removing-expandable-transcatheter-aortic-valve-implantation-remove-old-734390344.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Transcatheter Aortic Valve Replacement (TAVR): </h5>
                                <p>Dr Srinivasa Prasad is a master in performing Transcatheter aortic valve replacement (TAVR). It is a minimally invasive procedure performed on patients who are considered very risky for performing open heart surgery. In this procedure, a new aortic valve is transplanted without the requirement of removing the old one with the help of a specialised catheter.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Paravalvular Leak Device Closure: </h5>
                                <p>It is a widely covered feature when it comes to structural heart disease. One experiences this condition after valvular replacement and Dr. Srinivasa excels in smooth execution of this process.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Alcohol Septal Ablation: </h5>
                                <p>A minimally invasive and percutaneous procedure to help the patients with severe HCM, Dr Prasad being a globally renowned interventional cardiologist performs this procedure on his patients to help them.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://image.shutterstock.com/z/stock-photo-the-common-cardiac-embolism-as-as-source-of-ischemic-stroke-atrial-fibrillation-pfo-ventricular-1227447172.jpg">
                            </div>
                            <div class="col-lg-8">
                                <h5>Balloon Atrial Valvotomy: </h5>
                                <p>A minimally invasive procedure performed on people with mitral valve stenosis, Dr Prasad offers this treatment to treat the complications in his patients.</p>
                            </div>
                        </div>
                        <hr class="dash-border">
                        <div class="row">
                            <div class="col-lg-4 service-img">
                                <img src="https://via.placeholder.com/300x200">
                            </div>
                            <div class="col-lg-8">
                                <h5>Balloon Aortic Valvotomy: </h5>
                                <p>The process involves widening the patient’s aortic valve that with the use of a balloon catheter. To excel in this complicated process, only a connoisseur can and Dr Prasad has proved his excellence time and again.</p>
                            </div>
                        </div>
                        <hr class="dash-border">

                        <p>An astute student and an even more brilliant doctor with several scholarly articles as well as awards to his name, there is no end to the treatment procedures that Dr Prasad offers. From winning the very first prize in April 2014 to earning numerous other high-esteem certifications, he has been adept to his skills and has given his all to eradicate any health issues. Above all, he constantly works hard in adopting new technologies and processes to help his patients to live a happy life. The human heart is complicated but Dr Srinivasa Prasad tries heart and soul to help the people with heart diseases and lead the life that they desire. With the motto of gifting every individual a happy and healthy life, Dr Srinivasa Prasad is on the mission that he wants to succeed on. </p>
                        <p>Trust in Dr Srinivasa’s vision and take his aid whenever need!</p>

                        <?php /* <div class="row service-row">
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-medkit" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">First Aid Treatment</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-tint" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">Blood Bank</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-wheelchair" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">For Disable</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-heartbeat" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">Heart Specialist</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">Eye Care Specialist</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="services-box">
                                    <div class="service-icon"><a href="#"><i class="fa fa-h-square" aria-hidden="true"></i></a></div>
                                    <h4><a href="#">Multi Speciality</a></h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta luctus est interdum</p>
                                </div>
                            </div>
                        </div> */ ?>
                    </div>

                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </section>
    </div>

    <?php
        require("includes/footer.php");
    ?>
</body>
</html>