<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Awards | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Awards</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Awards</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">
                                    <h5>One award in 2014</h5>
                                    <ul>
                                        <li>Dr Srinivasa Prasad won the first prize in a reputed quiz competition held by Kerala Heart rhythm Society annual Conference in the year 2014.</li> 
                                        <li>Honoured with three respectable awards in 20 15</li>
                                        <li>He was also awarded Best Abstract Award at Interventional Cardiology Council, Kerala in 2015.</li>
                                        <li>Dr Prasad was also felicitated with the Best Paper Presentation Award at Indian College of Cardiology annual conference in the very same year. </li>
                                        <li>Proud owner as he Won Best Abstract award at Indian Heart Rhythm Society 7th  annual conference November 2015.</li>
                                    </ul>

                                    <h5>Received five high-esteem awards in 2016</h5>
                                    <ul>
                                        <li>Won First prize at Torrent Young Scholar Award 2016 – Cardiology  – Zonal level (south), conducted at Bangalore on 21 August 2016.</li>
                                        <li>Won Best Paper presentation Award at Indian College of Cardiology annual conference, conducted at Goa, September 2016. </li>
                                        <li>Won 3rd prize at Torrent Young Scholar Award 2016  – Cardiology – National level, conducted at Ahmedabad on 25 September 2016.</li>
                                        <li>Won Cardiology Young Scholar Award by obtaining First prize in Quiz at CSI Kerala chapter annual conference November 2016, conducted at Kochi.</li>
                                        <li>Won Best Abstract award at Cardiology Society of India annual national conference 2016, conducted at Kochi, in December 2016.</li>
                                    </ul>

                                    <h5>Winner of two awards in 2017.</h5>
                                    <ul>
                                        <li>The winner of Best case award at the 5th annual conference of Society for Heart Failure and Transplantation which took place in Calicut in between 23rd to 24th September in the year 2017.</li> 
                                        <li>Won Best paper award at Interventional Cardiology Council of Kerala annual conference held at Bekal on 6th  August 2017.</li>
                                    </ul>
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
</body>
</html>