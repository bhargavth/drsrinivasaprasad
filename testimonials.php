<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title>Testimonials | Dr. Srinivasa Prasad</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <!--[if lt IE 9]>
		<script src="assets/js/html5shiv.min.js"></script>
		<script src="assets/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

    <?php require("includes/header.php"); ?>

    <!-- Content -->
    <div class="main-content">

        <!-- Page Header -->
        <div class="page-header inner-banner">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title">
                            <span>Testimonials</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="content inner-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="blog-view">
                            <article class="blog blog-single-post">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="section-header">
                                            <h3 class="header-title">Testimonials</h3>
                                            <div class="line"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="blog-content">
                                    Coming Soon
                                </div>
                            </article>
                        </div>
                    </div>
                    <?php require("includes/sidebar.php"); ?>
                </div>
            </div>
        </div>
    </div>

    <?php require("includes/footer.php"); ?>
</body>
</html>